name             'dbsync'
maintainer       'Avankia LLC'
maintainer_email 'kiran.h@avankia.com'
license          'All rights reserved'
description      'Installs/Configures dbsync'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.1'
