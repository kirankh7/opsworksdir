#
# Cookbook Name:: dbsync
# Recipe:: default
#
# Copyright 2016, Avankia LLC
#
# All rights reserved - Do Not Redistribute
#
#DBSync CloudWorkFlow Installation

#Pre-Configs

#Install apache server
package ['httpd','mod_ssl'] do
  action :install 
end

package ['git'] do
  action :install 
end

service "httpd" do
  action [:start, :enable]
end

#Install OpenJDK 1.8 & Remove java package 

package ['java']  do
  action :remove
end

package ['java-1.8.0-openjdk'] do
  action :install
end

directory '/home/dbsync' do
  action :create
end

#Create a new user called "dbsync" for pakage installation
user 'dbsync' do
  comment 'Installation User for DBSync'
  manage_home true
  home '/home/dbsync'
  shell '/bin/bash'
  password 'redhat'
end
#create dir for installing DBSync
directory '/home/dbsync/server-1' do
  owner 'dbsync'
  group 'dbsync'
  mode '0755'
  action :create
end

directory '/home/dbsync/server-2' do
  owner 'dbsync'
  group 'dbsync'
  mode '0755'
  action :create
end
#Download Source code from 'git'
git '/home/dbsync/server-1' do
  repository 'https://bitbucket.org/dbsyncrelease/dbsync-ipaas.git'
  revision 'master'
  action :sync
end

git '/home/dbsync/server-2' do
  repository 'https://bitbucket.org/dbsyncrelease/dbsync-ipaas.git'
  revision 'master'
  action :sync
end

##I guess we can use few ways to fix this try as you like 
#execute 'cp -a /home/dbsync/server/dbsync-ipass /home/dbsync/server/dbsync-ipass-1 ; mv /home/dbsync/server/dbsync-ipass /home/dbsync/server/dbsync-ipass-2'

#Create template to load httpd configuration
template '/etc/httpd/conf.d/lb.conf' do
  action :create
  source 'lb.conf.erb'
end

#Configuring second tomcat with different port numbers
template '/home/dbsync/server-2/server/conf/server.xml' do
  source 'server.xml.erb'
end

#Configuring system.properties
template '/home/dbsync/server-1/server/CloudWF/WEB-INF/conf/system.property' do
  source 'system.property.erb'
end


